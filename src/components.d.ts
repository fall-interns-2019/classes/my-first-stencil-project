/* tslint:disable */
/**
 * This is an autogenerated file created by the Stencil compiler.
 * It contains typing information for all components that exist in this project.
 */


import { HTMLStencilElement, JSXBase } from '@stencil/core/internal';


export namespace Components {
  interface AppHome {}
  interface AppProfile {
    'name': string;
  }
  interface AppRoot {}
  interface MyHome {
    'name': string;
    'text': string;
  }
  interface PersonItem {
    'person': {first_name: string, last_name: string};
  }
}

declare global {


  interface HTMLAppHomeElement extends Components.AppHome, HTMLStencilElement {}
  var HTMLAppHomeElement: {
    prototype: HTMLAppHomeElement;
    new (): HTMLAppHomeElement;
  };

  interface HTMLAppProfileElement extends Components.AppProfile, HTMLStencilElement {}
  var HTMLAppProfileElement: {
    prototype: HTMLAppProfileElement;
    new (): HTMLAppProfileElement;
  };

  interface HTMLAppRootElement extends Components.AppRoot, HTMLStencilElement {}
  var HTMLAppRootElement: {
    prototype: HTMLAppRootElement;
    new (): HTMLAppRootElement;
  };

  interface HTMLMyHomeElement extends Components.MyHome, HTMLStencilElement {}
  var HTMLMyHomeElement: {
    prototype: HTMLMyHomeElement;
    new (): HTMLMyHomeElement;
  };

  interface HTMLPersonItemElement extends Components.PersonItem, HTMLStencilElement {}
  var HTMLPersonItemElement: {
    prototype: HTMLPersonItemElement;
    new (): HTMLPersonItemElement;
  };
  interface HTMLElementTagNameMap {
    'app-home': HTMLAppHomeElement;
    'app-profile': HTMLAppProfileElement;
    'app-root': HTMLAppRootElement;
    'my-home': HTMLMyHomeElement;
    'person-item': HTMLPersonItemElement;
  }
}

declare namespace LocalJSX {
  interface AppHome extends JSXBase.HTMLAttributes<HTMLAppHomeElement> {}
  interface AppProfile extends JSXBase.HTMLAttributes<HTMLAppProfileElement> {
    'name'?: string;
  }
  interface AppRoot extends JSXBase.HTMLAttributes<HTMLAppRootElement> {}
  interface MyHome extends JSXBase.HTMLAttributes<HTMLMyHomeElement> {
    'name'?: string;
    'text'?: string;
  }
  interface PersonItem extends JSXBase.HTMLAttributes<HTMLPersonItemElement> {
    'onShowName'?: (event: CustomEvent<any>) => void;
    'person'?: {first_name: string, last_name: string};
  }

  interface IntrinsicElements {
    'app-home': AppHome;
    'app-profile': AppProfile;
    'app-root': AppRoot;
    'my-home': MyHome;
    'person-item': PersonItem;
  }
}

export { LocalJSX as JSX };


declare module "@stencil/core" {
  export namespace JSX {
    interface IntrinsicElements extends LocalJSX.IntrinsicElements {}
  }
}


