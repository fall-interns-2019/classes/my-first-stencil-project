import {Component, Event, EventEmitter, h, Prop} from '@stencil/core';

@Component({
  tag: 'person-item',
})
export class PersonItem {
  @Prop() person: {first_name: string, last_name: string};
  @Event() showName: EventEmitter;

  onChange() {
    this.showName.emit(this.person)
  }

  render() {
    return [
      <ion-item onClick={() => this.onChange() } >
        <ion-label>
          {this.person.first_name} {this.person.last_name}
        </ion-label>
      </ion-item>
    ];
  }
}
