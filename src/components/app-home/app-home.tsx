import {Component, h, Listen, State} from '@stencil/core';

@Component({
  tag: 'app-home',
})
export class AppHome {
  people = [ {first_name: 'sam', last_name: 'birk'}, {first_name: 'josh', last_name: 'birk'}];
  @State() showPeople = true;
  @State() currentTitle = 'Home';
  renderPeople() {
    if(!this.showPeople) { return }
    return this.people.map((person) => {
      return [
        <person-item person={person} />
      ]
    })
  }

  @Listen('showName')
  sdfdf(event) {
    this.currentTitle = `${event.detail.first_name} ${event.detail.last_name}`;
  }

  toggleShowPeople() {
    this.showPeople = !this.showPeople;
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar color="primary">
          <ion-title>{this.currentTitle}</ion-title>
        </ion-toolbar>
      </ion-header>,

      <ion-content class="ion-padding">
        {this.renderPeople()}
        <ion-button onClick={() => this.toggleShowPeople()}>
          Show/Hide People
        </ion-button>
      </ion-content>
    ];
  }
}
