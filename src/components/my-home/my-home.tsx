import { Component, h, Prop } from '@stencil/core';

@Component({
  tag: 'my-home',
})
export class MyHome {
  @Prop() text: string;
  @Prop() name: string;

  render() {
    return [
     <h1>
       {this.text} {this.name}
     </h1>
    ];
  }
}
